
public class MercedesFactory extends Factory {

	@Override
	public HeadLight makeHeadLight() {
		return new MercedesHeadLight();
	}
	
	@Override
	public Tire makeTire() {
		return new MercedesTire();
	}
}
