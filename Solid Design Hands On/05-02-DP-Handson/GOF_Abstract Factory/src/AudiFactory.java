
public class AudiFactory extends Factory {

	@Override
	public HeadLight makeHeadLight() {
		return new AudiHeadLight();
	}
	
	@Override
	public Tire makeTire() {
		return new AudiTire();
	}
}
