import java.util.Scanner;

public class Client {
	
	public static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter brand either Audi or Mercedes :");
		String brand = sc.nextLine();
		Factory factory; 
		if(brand.equalsIgnoreCase("Audi")) {
			factory = new  AudiFactory();
		}
		else {
			factory = new MercedesFactory();
		}
		HeadLight headLight = factory.makeHeadLight();
		Tire tire = factory.makeTire();
		System.out.println(headLight.headLightType());
		System.out.println(tire.tireType());
	}

}
