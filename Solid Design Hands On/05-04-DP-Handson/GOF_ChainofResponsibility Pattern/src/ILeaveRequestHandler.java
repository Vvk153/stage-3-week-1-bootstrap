
public interface ILeaveRequestHandler {

	ILeaveRequestHandler nextHandler = new Supervisor();
	
	void handleRequest(LeaveRequest leaveRequest);
}
