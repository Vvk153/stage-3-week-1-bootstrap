import java.util.Scanner;

public class Program {

	public static Scanner sc = new Scanner(System.in);
	public static void main(String args[]) {
		LeaveRequest leaveRequest = new LeaveRequest();
		System.out.println("Enter Employee Name");
		leaveRequest.setEmployee(sc.nextLine());
		System.out.println("Enter no of leave days");
		leaveRequest.setLeaveDays(sc.nextInt());
		ILeaveRequestHandler handler = new Supervisor();
		handler.handleRequest(leaveRequest);
	}
}
