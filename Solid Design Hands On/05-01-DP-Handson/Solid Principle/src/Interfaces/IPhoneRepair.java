package Interfaces;

public interface IPhoneRepair {

	void processPhoneRepair(String modelName);
}
