package com.cognizant.moviecruiser.dao;

import java.util.HashMap;

import com.cognizant.moviecruiser.model.Favorites;

public class FavoritesDaoCollectionImpl  {
	private static HashMap<Long, Favorites> userFavorites;

	public FavoritesDaoCollectionImpl() {
		super();
		if (userFavorites == null) {
			userFavorites = new HashMap<Long, Favorites>();
		}
	}
}
