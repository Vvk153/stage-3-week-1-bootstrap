import java.io.*;
import java.util.*;

public class Solution {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Stack<Integer> stack1 = new Stack<Integer>();
        Stack<Integer> stack2 = new Stack<Integer>();
        int query = sc.nextInt();
        int temp = 0;
        int number = 0;
        // for(int i = 0; i < query; i++) {
        //     temp = sc.nextInt();
        //     switch(temp) {
        //         case 1: 
        //             number = sc.nextInt();
        //             if(stack1.isEmpty()) {
        //                 stack1.push(number);
        //             }
        //             else {
        //                 while(!stack1.isEmpty()) {
        //                     stack2.push(stack1.pop());
        //                 }
        //                 stack1.push(number);
        //                 while(!stack2.isEmpty()) {
        //                     stack1.push(stack2.pop());
        //                 }
        //             }
        //             break;
        //         case 2:
        //             stack1.pop();
        //             break;
        //         case 3:
        //             System.out.println(stack1.peek());
        //             break;
        //     }
        // }
        for(int i = 0; i < query; i++) {
            temp = sc.nextInt();
            switch(temp) {
                case 1: 
                    number = sc.nextInt();
                    stack1.push(number);
                    break;
                case 2:
                    if(stack2.isEmpty()) {
                        while(!stack1.isEmpty()) {
                            stack2.push(stack1.pop());
                        }
                    }
                    stack2.pop();
                    break;
                case 3:
                    if(stack2.isEmpty()) {
                        while(!stack1.isEmpty()) {
                            stack2.push(stack1.pop());
                        }
                    }
                    System.out.println(stack2.peek());
                    break;
            }
        } 
    }
}