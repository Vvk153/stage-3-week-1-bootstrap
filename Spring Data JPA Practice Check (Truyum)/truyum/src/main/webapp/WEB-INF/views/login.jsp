<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Login Page</title>
<!-- Bootstrap CSS -->
    

<%-- <link href='<c:url value = "/truyum/src/main/webapp/resources/css/style.css" />' rel="stylesheet"> --%>
<!-- <link rel="stylesheet" href="css/style.css" type="text/css"> -->
<link rel="stylesheet"	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css"
	rel="stylesheet" type="text/css" />
	<link  href="/css/style.css"
     rel="stylesheet">
</head>
<body>
	<!-- Nav Bar -->
	<nav class="navbar navbar-expand-sm navbar-dark"
		style="background-color: coral">
		<!-- Nav Bar Branding -->
		<a class="navbar-brand" href="menu-item-list.html"><span
			class="material-icons material-logo mr-2"> restaurant_menu </span>truYum</a>
		<!-- Nav Bar Toggle Buttom -->
		<button class="navbar-toggler d-lg-none" type="button"
			data-toggle="collapse" data-target="#collapsibleNavId"
			aria-controls="collapsibleNavId" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- Nav Bar Options -->
		<div class="collapse navbar-collapse" id="collapsibleNavId"></div>
	</nav>
	<!-- End Nav Bar -->

	<!-- Main Body -->
	<div class="container-fulid m-5">
		<div class="row">
			<div class="col-md-6 offset-md-3 text-center">
				<h1>Login</h1>
				<form action="">
					<div class="row p-2 text-dark">
						<div class="col-12 p-3">
							<label for=""><b>UserName</b></label> <input type="text"
								name="username" id="username" class="form-control text-center"
								placeholder="Enter Username" required>
							<div class="invalid-feedback">Username is mandatory</div>
						</div>
						<div class="col-12 p-3">
							<label for=""><b>Password</b></label> <input type="password"
								name="password" id="password" class="form-control text-center"
								placeholder="Enter Password" required>
							<div class="invalid-feedback">Password is mandatory</div>
						</div>
						<div class="col-12 p-3">
							<button class="btn btn-save w-50" type="submit">Login</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- End Main Body -->

	<!-- Footer -->
	<footer class="page-footer font-small position-bottom "
		style="background-color: coral">
		<div class="footer-copyright text-left py-1 ml-3 text-white">Copyright
			2019</div>
	</footer>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>