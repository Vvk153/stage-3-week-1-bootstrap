<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Add Menu Item</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css"
	rel="stylesheet" type="text/css" />
<link  href="/css/style.css"
     rel="stylesheet">

</head>
<body>
	<!-- Nav Bar -->
	<nav class="navbar navbar-expand-sm navbar-dark"
		style="background-color: coral">
		<!-- Nav Bar Branding -->
		<a class="navbar-brand" href="menu-item-list.html"><span
			class="material-icons material-logo mr-2"> restaurant_menu </span>truYum</a>
		<!-- Nav Bar Toggle Buttom -->
		<button class="navbar-toggler d-lg-none" type="button"
			data-toggle="collapse" data-target="#collapsibleNavId"
			aria-controls="collapsibleNavId" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- Nav Bar Options -->
		<div class="collapse navbar-collapse" id="collapsibleNavId">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a class="nav-link"
					href="menu-item-list.html">Menu Items</a></li>
			</ul>
		</div>
	</nav>
	<!-- End Nav Bar -->

	<!-- Main Body -->
	<div class="container-fulid m-5">
		<h2>Edit Menu Item</h2>
		<form:form action="" method="post" modelAttribute="menu">
			<div class="row mb-2">
				<div class="col-sm-12 col-xl-12 form-group">
					<form:label path="name" for="name">Item Name</form:label>
					<form:input type="text" path="name" name="name" id="name"
						class="form-control" placeholder="" required="required" />
					<form:errors path="name" id="name" name="name"
						class="invalid-feedback">Item Name is required</form:errors>
					<form:errors path="name" id="name" name="name"
						class="invalid-feedback">Item Name cannot exceed 200
						characters</form:errors>
				</div>
			</div>
			<div class="row mb-2">
				<div class="col-sm-12 col-xl-3 form-group">
					<form:label path="price" for="price">Price</form:label>
					<form:input path="price" type="text" name="price" id="price"
						class="form-control" placeholder="" required="required" />
					<form:errors path="price" class="invalid-feedback">Price is required</form:errors>
					<form:errors path="price" class="invalid-feedback">Price has to be number</form:errors>
				</div>
				<div class="col-sm-12 col-xl-3 form-group">
					<form:label path="launchDate" for="launchDate">Date of Launch</form:label>
					<form:input type="text" path="launchDate" name="launchDate"
						id="launchDate" class="form-control" placeholder="" required="required" />
					<form:errors path="launchDate" class="invalid-feedback">Date of Launch is required</form:errors>
					<form:errors path="launchDate" class="invalid-feedback">Price has to be number</form:errors>
				</div>
				<div class="col-sm-12 col-xl-3 form-group">
					<form:label path="category" for="category">Category</form:label>
					<form:select class="form-control" path="category">
						<form:option value="Main Course">Main Course</form:option>
						<form:option value="Dessert">Dessert</form:option>
						<form:option value="Starter">Starter</form:option>
						<form:option value="Snacks">Snacks</form:option>
					</form:select>
					
					<!-- <select class="form-control" id="category" required="required">
						<option value=""></option>
						<option value=""></option>
						<option value=""></option>
					</select> -->
					<form:errors path="category" class="invalid-feedback">Select one category</form:errors>
				</div>
				<div class="col-sm-12 col-xl-3 form-group">
					<form:label path="active" for="active">Active</form:label>
					<div>
						<form:radiobutton path="active" class="radio-inline " value="true"/>Yes
						&nbsp;&nbsp;&nbsp;
						<form:radiobutton path="active" class="radio-inline " value="false"/>No
						<%-- <form:label path="active" class="radio-inline ">
							<form:input path="active" type="radio" name="Yes" value="true" />Yes</form:label>
						&nbsp;&nbsp;&nbsp;
						<form:label path="active" class="radio-inline">
							<form:input path="active" type="radio" name="No" value="false" />No</form:label> --%>
					</div>
					<form:errors path="active" class="invalid-feedback">Select an active status</form:errors>
				</div>
			</div>
			<div class="row mb-2">
				<div class="col-sm-12 col-xl-6">
					<div class="checkbox">
						<form:checkbox path="freeDelivery"/>&nbsp;&nbsp;Free
							Delivery
						<%-- <form:label path="freeDelivery">
							<form:input path="freeDelivery" type="checkbox"
								value="Free Delivery" />&nbsp;&nbsp;Free
							Delivery</form:label> --%>
					</div>
				</div>
				<div class="col-sm-12 col-xl-6 form-group">
					<form:label path="imageUrl" for="price">Image URL</form:label>
					<form:input path="imageUrl" type="text" name="imageUrl"
						id="imageUrl" class="form-control" placeholder="" />
				</div>
			</div>
			<div class="row mb-2">
				<div class="col-12">
					<form:button class="btn btn-save" type="submit">Save</form:button>
				</div>
			</div>
		</form:form>
	</div>
	<!-- End Main Body -->

	<!-- Footer -->
	<footer class="page-footer font-small position-bottom "
		style="background-color: coral">
		<div class="footer-copyright text-left py-1 ml-3 text-white">Copyright
			2019</div>
	</footer>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"
		type="text/javascript"></script>

</body>
<script>

    $('#launchDate').datepicker({
    
    uiLibrary: 'bootstrap4',
    
    format: 'dd/mm/yyyy'
    
    });
    
    </script>
</html>