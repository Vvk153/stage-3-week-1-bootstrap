package com.cognizant.truyum.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cognizant.truyum.model.Menu;

public interface MenuRepository extends JpaRepository<Menu, Integer> {

	public List<Menu> findByActiveTrueAndLaunchDateLessThanEqual(Date date);
}
