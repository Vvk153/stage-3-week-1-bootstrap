package com.cognizant.solidDesign;

public abstract class Order {

	public String channel;
	
	public String productType;
	
	public abstract void processOrder();
}
