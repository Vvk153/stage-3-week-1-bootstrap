package com.cognizant.solidDesign;

import java.util.Scanner;

public class Clint {

	public static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter Channel Mode either E-Commerce Or Tele Caller");
		String channel = sc.nextLine();
		Factory factory;
		if(channel.equalsIgnoreCase("E-Commerce")) {
			factory = new ECommerceFactory();
		}
		else {
			factory = new TeleCallerAgentFactory();
		}
		System.out.println("Enter Order Type: Toy, Electronic, Furniture");
		String orderType = sc.nextLine();
		Order order;
		switch (orderType) {
		case "Toy":
			order = new ToysOrder();
			break;
		case "Electronic":
			order = new ElectronicOrder();
			break;
		case "Furniture":
			order = new FurnitureOrder();
			break;
			default :
				order = new Order() {
					
					@Override
					public void processOrder() {
						// TODO Auto-generated method stub
						System.out.println("No Order Selected");
					}
				};
		}
		order.processOrder();
	}

}
