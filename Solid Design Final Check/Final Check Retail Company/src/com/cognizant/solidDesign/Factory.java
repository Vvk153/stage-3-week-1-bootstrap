package com.cognizant.solidDesign;

public abstract class Factory {

	public abstract ElectronicOrder getElectronicOrder();
	
	public abstract FurnitureOrder getFurnitureOrder();
	
	public abstract ToysOrder getToysOrder();
	
}
