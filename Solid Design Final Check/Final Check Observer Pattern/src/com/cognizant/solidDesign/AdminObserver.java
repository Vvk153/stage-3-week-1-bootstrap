package com.cognizant.solidDesign;

public class AdminObserver implements INotificationObserver {

	public void update(String eventName, int ticketCount) {

		System.out.println("Notification received for event " + eventName + ". Ticked Booked = " + ticketCount);
	}
}