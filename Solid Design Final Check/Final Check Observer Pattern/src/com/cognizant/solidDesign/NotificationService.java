package com.cognizant.solidDesign;

import java.util.ArrayList;

public class NotificationService implements INotificationService {

//	private ArrayList<INotificationObserver> observers = new ArrayList<INotificationObserver>();

	private INotificationObserver observer;
	

	public INotificationObserver getObserver() {
		return observer;
	}

	public void setObserver(INotificationObserver observer) {
		this.observer = observer;
	}

	public void notifyObservers(Event event) {
		System.out.println("Notification");
		observer.update(event.getEventName(),event.getTicketCount());
	}

	public void registerObserver(INotificationObserver observer) {
		this.observer = observer;
	}

	public void removeObserver(INotificationObserver observer) {
		observer = null;
	}

}