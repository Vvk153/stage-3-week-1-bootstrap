package com.cognizant.solidDesign;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Event event = new SomeEvent();
		INotificationObserver adminObserver = new AdminObserver();
		INotificationService notificationService = new NotificationService();
		notificationService.registerObserver(adminObserver);
		notificationService.notifyObservers(event);
	}

}
