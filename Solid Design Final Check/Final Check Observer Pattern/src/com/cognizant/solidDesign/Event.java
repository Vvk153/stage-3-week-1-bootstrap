package com.cognizant.solidDesign;

public interface Event {

	public String getEventName();
	
	public int getTicketCount();
}
