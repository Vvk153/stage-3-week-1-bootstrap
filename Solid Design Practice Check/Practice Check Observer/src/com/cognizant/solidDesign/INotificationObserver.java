package com.cognizant.solidDesign;

public interface INotificationObserver {
	public void update();
}
